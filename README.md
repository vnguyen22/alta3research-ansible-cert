# alta3research-ansible-cert

#Purpose
The purpose of this script is to shut off the application, deploy some files from a build, and then turn the application back on.

This is a process that we would run fairly often. At least multiple times a week between multiple different environments.

#Important Information
This is run using the playbook: playbook-deploybuild.yml. It uses 3 roles to complete the necessary tasks: stop_app, start_app, and deploy.

When running the playbook it is required to add: -e "build_version=#". Where # can be 1, 2, or 3. I only staged 3 "builds".

By default this playbook will look for a "changeme" build which does not exist to prevent accidental deployment of a build.

#Additional Information
Additional conditionals that can be added to the command line for various effects:

dont_start: setting this to true will leave the application down after updating

force_build: setting this to true will force the build to run
